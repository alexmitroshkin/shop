package ru.mitroshkinam.auth;

import ru.mitroshkinam.auth.domain.ShopUser;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebFilter(urlPatterns = {LoginFilter.USER_FILTER_URI + "*", LoginFilter.ADMIN_FILTER_URI + "*"})
public class LoginFilter implements Filter{

    public static final String USER_FILTER_URI = "/user/";
    public static final String ADMIN_FILTER_URI = "/admin/";

    @Inject
    private AuthBean authBean;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpServletRequest request = (HttpServletRequest) servletRequest;

        ShopUser.Role role = authBean.getRole();
        if (role != null){
            String uri = request.getRequestURI();
            String beginOfAdminURI = request.getContextPath() + ADMIN_FILTER_URI;
            if (uri.startsWith(beginOfAdminURI) && role != ShopUser.Role.ADMIN){
                response.sendRedirect(request.getContextPath() + "/errors.xhtml");
                return;
            }

            chain.doFilter(servletRequest, servletResponse);
            return;
        }

        authBean.setRequestedPage(request.getRequestURI());
        response.sendRedirect(request.getContextPath() + "/login.xhtml");
    }

    @Override
    public void destroy() {

    }
}
