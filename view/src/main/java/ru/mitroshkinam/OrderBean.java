package ru.mitroshkinam;

import ru.mitroshkinam.domain.Order;
import ru.mitroshkinam.domain.Product;
import ru.mitroshkinam.ejb.OrdersManagerBean;
import ru.mitroshkinam.ejb.ProductsManagerBean;

import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

@Named
@SessionScoped
public class OrderBean implements Serializable {

    private Order order;
    private String name;
    private int price;
    private int quantity;

    @EJB
    private OrdersManagerBean ordersManagerBean;
    @EJB
    private ProductsManagerBean productsManagerBean;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void createOrder(){
        if (order == null){
            order = ordersManagerBean.creteOrder();
        }
    }

    public void createProduct(){
        productsManagerBean.createProduct(name, quantity);
    }

    public List<Product> getProducts(){
        return productsManagerBean.getProducts();
    }

    public void addProductToOrder(Product product){
        if (order == null) {
            return;
        }
        ordersManagerBean.addProductToOrder(product.getId(), order.getId(), quantity);
    }

    public List<Product> getProductsInOrder(){
        if (order == null){
            return Collections.emptyList();
        }
        return ordersManagerBean.getProductsInOrder(order.getId());
    }
}
