package ru.mitroshkinam.ejb;

import ru.mitroshkinam.domain.Order;
import ru.mitroshkinam.domain.Product;
import ru.mitroshkinam.domain.ProductInOrder;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Stateless
@LocalBean
public class OrdersManagerBean {

    @PersistenceContext(unitName = "shopPU")
    private EntityManager entityManager;

    public Order creteOrder(){
        Order order = new Order();
        entityManager.persist(order);
        return order;
    }

    public boolean addProductToOrder(long productId, long orderId, int quantity){
        Product product = entityManager.find(Product.class, productId);
        if (product == null) {
            return false;
        }

        Order order = entityManager.find(Order.class, orderId);
        if (order == null) {
            return  false;
        }

        ProductInOrder productInOrder = new ProductInOrder();
        productInOrder.setProduct(product);
        productInOrder.setOrder(order);
        productInOrder.setQuantity(quantity);
        entityManager.persist(productInOrder);
        return true;
    }

    public List<Product> getProductsInOrder(long orderId){
        Order order = entityManager.find(Order.class, orderId);
        if (order == null) {
            return Collections.emptyList();
        }
        List<ProductInOrder> productInOrders = order.getProductInOrders();
        List<Product> result = productInOrders.stream().map(ProductInOrder::getProduct).collect(Collectors.toList());
        return result;
    }
}
